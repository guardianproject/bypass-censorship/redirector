SHELL := /bin/bash
LOCALES = es pl ru pt_BR ro ru tok tr

-include $(shell curl -sSL -o .build-harness-ext "https://gitlab.com/sr2c/build-harness-extensions/-/raw/main/Makefile.bootstrap"; echo .build-harness-ext)

lint: python/mypy python/bandit

translations/update:
	pybabel extract -F babel.cfg -o app/i18n/messages.pot .
	for locale in $(LOCALES); do \
		pybabel update -i app/i18n/messages.pot -d app/i18n -l $$locale; \
	done

translations/compile:
	pybabel compile -d app/i18n
