from typing import Tuple
from unittest.mock import patch

import geoip2
import pytest
from flask import Flask

from app.redirector import client_needs_mirror, parse_url


@pytest.mark.parametrize(
    "url, expected_output",
    [
        (
            "https://www.example.com/path/to/resource?param1=value1&param2=value2",
            ("www.example.com", "/path/to/resource?param1=value1&param2=value2"),
        ),
        (
            "https://www.example.com/path/to/resource",
            ("www.example.com", "/path/to/resource"),
        ),
        (
            "https://www.example.com?param1=value1&param2=value2",
            ("www.example.com", "/?param1=value1&param2=value2"),
        ),
        (
            "https://www.example.com",
            ("www.example.com", "/"),
        ),
        (
            "https://www.example.com:8080/path/to/resource",
            ("www.example.com", "/path/to/resource"),
        ),
        (
            "https://WWW.EXAMPLE.COM/path/to/resource",
            ("www.example.com", "/path/to/resource"),
        ),
        (
            "https://www.example.com/example_with_force?hello=yes&force=true",
            ("www.example.com", "/example_with_force?hello=yes")
        ),
        (
            "https://www.example.com/example_with_force?force_ip=192.0.2.0&hello=yes",
            ("www.example.com", "/example_with_force?hello=yes")
        )
    ],
)
def test_parse_url(url: str, expected_output: Tuple[str, str]) -> None:
    assert parse_url(url) == expected_output


@pytest.mark.parametrize(
    "client_ip, expected_output",
    [
        (None, True),  # Client IP is None
        ("192.0.2.1", True),  # IP address not found in database
        ("192.0.2.2", False),  # IP address associated with a country NOT in the MIRROR_COUNTRIES list
        ("203.0.113.1", True),  # IP address without country association
        ("198.51.100.1", True),  # IP address associated with a country in the MIRROR_COUNTRIES list
    ],
)
def test_client_needs_mirror(client_ip: str, expected_output: bool) -> None:
    test_app = Flask("test")
    test_app.config["MIRROR_COUNTRIES"] = ["ZZ"]
    with test_app.app_context():
        # Mock the geoip_reader object to return a mocked result
        with patch("app.extensions.geoip_reader.geoip_reader") as mock_geoip_reader:
            # Set up the mock return value based on the test case
            if client_ip == "192.0.2.1":
                # IP address not found in database
                mock_geoip_reader.country.side_effect = geoip2.errors.AddressNotFoundError("Not found")
            elif client_ip == "203.0.113.1":
                # IP address with no country association
                mock_geoip_reader.country.return_value.country.iso_code = None
            elif client_ip == "192.0.2.2":
                # IP address associated with a good country
                mock_geoip_reader.country.return_value.country.iso_code = "AQ"
            else:
                # IP address associated with a country that needs a mirror
                mock_geoip_reader.country.return_value.country.iso_code = "ZZ"

            # Call the function under test
            result = client_needs_mirror(client_ip)

        # Assert the result matches the expected output
        assert result == expected_output
