import json
import logging
from typing import TYPE_CHECKING, Dict, Optional, Union, List

Pool = Dict[str, Union[Dict[str, str], str]]
PoolData = Optional[Dict[str, Union[str, List[Pool]]]]

data: PoolData = None


def load_initial_data(data_filename: Optional[str] = None) -> None:
    """
    Load initial data from a JSON file, by default `data.json` in the working directory.

    This function reads the contents of the file and assigns it to the global variable `data`.
    It also logs a debug message indicating that the initial data has been loaded from the filesystem.

    :return: None
    """
    global data
    data = json.load(open(data_filename or "data.json"))
    logging.debug("Loaded initial data from filesystem")


def get_data() -> PoolData:
    global data
    return data


def update_data(new_data: PoolData) -> None:
    global data
    data = new_data


def lookup_pool(key: Optional[str] = None,
                name: Optional[str] = None) -> Optional[Pool]:
    """
    Searches for a pool that matches the provided API key or short name.

    The function will return `None` if the data is not loaded or if no key and name is provided.
    If a pool matches the provided API key or short name, it will be returned.

    :param key: The API key of the pool to search for.
    :param name: The short name of the pool to search for.
    :return: The pool matching the API key or short name, or `None` if not found.
    """
    global data
    if data is None:
        return None  # We don't know about the pools yet
    if key is None and name is None:
        return None  # Nothing is going to match nothing
    for pool in data['pools']:
        if TYPE_CHECKING:
            if isinstance(pool, str):
                raise RuntimeError("Invalid pool configuration")
        if key is not None and pool['api_key'] == key:
            return pool
        if name is not None and pool['short_name'] == name:
            return pool
    return None


def lookup_mirror(pool: Pool, origin_domain: str) -> Optional[str]:
    """
    Retrieves the origin corresponding to the provided domain name from the pool.

    :param pool: The pool of resources where the mirror should be found.
    :param origin_domain: The origin domain name to search for in the pool.
    :return: The origin of the domain if found, otherwise `None`.
    """
    if not pool:
        return None
    origins = pool["origins"]
    if TYPE_CHECKING:
        assert isinstance(origins, dict)  # nosec B101
    return origins.get(origin_domain, None)
