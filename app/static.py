import logging
from typing import Optional, List, Tuple, Union

import requests
from bs4 import BeautifulSoup
from google.api_core.exceptions import GoogleAPICallError
from google.cloud import storage


def get_opengraph_tags(url: str) -> Optional[List[Tuple[str, str]]]:
    """
    Fetch and parse OpenGraph, Facebook and Twitter meta tags from a given URL.

    This function sends a GET request to the provided URL and parses the response content
    for meta tags. It then checks the 'property' and 'name' attributes of these tags for
    OpenGraph ('og'), Facebook ('fb'), and Twitter ('twitter') tags.

    :param url: The URL of the webpage from which to fetch OpenGraph tags.
    :return: A list of tuples, where each tuple contains a tag (name or property) and its content.
             Returns None if an exception occurs during the GET request.
    """
    prefixes = ['og', 'fb', 'twitter']

    try:
        page = requests.get(url, timeout=10)
        page.raise_for_status()  # This will raise an HTTPError if the response was unsuccessful
    except requests.RequestException as e:
        logging.warning(f'Failed to retrieve webpage at url: {url}. Exception: {e}')
        return None

    soup = BeautifulSoup(page.content, "html.parser")
    all_meta_tags = soup.find_all('meta')
    result = []

    for tag in all_meta_tags:
        type_tag: Optional[str] = None
        if 'property' in tag.attrs:
            for keyword in prefixes:
                if keyword in tag['property']:
                    type_tag = 'property'
        elif 'name' in tag.attrs:
            for keyword in prefixes:
                if keyword in tag['name']:
                    type_tag = 'name'
        if type_tag is not None:
            result.append((tag[type_tag], tag["content"]))
    return result


def blob_to_bucket(storage_client: storage.Client,
                   bucket_name: str,
                   content: Union[bytes, str],
                   content_type: Optional[str],
                   destination_blob_name: str) -> bool:
    """
    Uploads a string as object content to the bucket.

    :param storage_client: The Google Cloud Storage client.
    :param bucket_name: The name of the bucket where the string will be uploaded.
    :param content: The string content to be uploaded.
    :param content_type: The type of the content being uploaded. Defaults to "text/html".
    :param destination_blob_name: The name of the blob where the string will be uploaded.
    :return: A boolean indicating whether the upload was successful.
    """
    try:
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(destination_blob_name)
        if not content_type:
            content_type = "text/html"
        blob.content_type = content_type
        blob.upload_from_string(content, content_type=content_type)
        return True
    except GoogleAPICallError as e:
        logging.warning(f'Failed to upload blob to bucket. Exception: {e}')
        return False


def delete_from_bucket(storage_client: storage.Client,
                       bucket_name: str,
                       blob_name: str) -> bool:
    try:
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(blob_name)
        blob.delete()
        return True
    except GoogleAPICallError as e:
        logging.warning(f'Failed to delete blob from bucket. Exception: {e}')
        return False
