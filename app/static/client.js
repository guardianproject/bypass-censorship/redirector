
const urlInput = document.getElementById("article-url");
const urlOutput = document.getElementById("generated-url");

const errorMessage = document.getElementById("error-message");

const copyTextToClipboard = function (text) {
  return navigator.clipboard.writeText(text).then(function () {
    console.info('Mirror URL is copied to clipboard');
    console.info(text);
    return true;
  }, function (err) {
    console.error('Unable to copy URL: ', err);
    return false;
  });
}

function executeRoutine(url) {
  let cacheExpirationKey = "cacheExpiration";
  let cachedDataKey = "cachedData";
  let expirationTime = 10 * 60 * 1000; // 10 minutes

  async function fetchJsonData() {
    return new Promise((resolve, reject) => {
      chrome.storage.local.get([cacheExpirationKey, cachedDataKey], function(result) {
        let now = Date.now();
        // If data is in cache and cache not expired, use it
        if (result[cacheExpirationKey] && now < result[cacheExpirationKey] && result[cachedDataKey]) {
          resolve(result[cachedDataKey]);
        } else {
          // Fetch data from URL
          fetch("https://cdn.jsdelivr.net/gh/OpenTechFund/bypass-mirrors@master/mirrorSites.json")
              .then(response => response.json())
              .then(data => {
                // Store fetched data in cache and set expiration time
                chrome.storage.local.set({
                  [cacheExpirationKey]: now + expirationTime,
                  [cachedDataKey]: data
                }, function() {
                  resolve(data);
                });
              })
              .catch(error => reject(error));
        }
      });
    });
  }

  async function generateFallbackUrl(originalUrl) {
    let data = await fetchJsonData();

    // Extract the hostname from the original URL
    let url = new URL(originalUrl);
    let originalDomain = url.hostname.replace(/^www\./, '');

    // Find the corresponding site
    let site = data.sites.find(site => site.main_domain === originalDomain);
    if (site) {
      // Find the alternative with proto "https"
      let mirrorAlternative = site.available_alternatives.find(alternative => alternative.proto === "https");
      if (mirrorAlternative) {
        // Replace the original domain with the mirror domain
        url.hostname = new URL(mirrorAlternative.url).hostname;
        return url.toString();
      } else {
        throw new Error("No mirror alternative found for the given URL.");
      }
    } else {
      throw new Error("No site found for the given URL: " + url);
    }
  }

  async function generateLink(originalUrl, type) {
    let key = new URLSearchParams(document.location.search).get("key");
    let apiUrl = `/link?url=${encodeURIComponent(originalUrl)}&type=${type}`;
    if (key !== null) {
      apiUrl += `&key=${key}`;
    }

    // Send a GET request to the API
    let response = await fetch(apiUrl);

    // Check if the request was successful
    if (!response.ok) {
      console.log(await response.text())
      throw new Error("HTTP error, status: " + response.status);
    }

    // Parse the JSON response
    let data = await response.json();

    // Check if the 'url' key is in the object
    if (!data.hasOwnProperty('url') || data.url.length < 5) {
      throw new Error("The 'url' key is not in the returned JSON object, or is obviously too short.");
    }

    // Return the private mirror URL from the response
    return data.url;
  }

  async function generateURL(url) {
    errorMessage.textContent = "Generating dedicated mirror link...";
    try {
      return await generateLink(url, "short");
    } catch (error) {
      console.log(error);
      errorMessage.textContent = "Failed. Trying to generate public mirror URL...";
      try {
        return await generateFallbackUrl(url);
      } catch (error) {
        console.log(error);
        errorMessage.textContent = "Failed. Trying to generate GetSiteCopy URL...";
        return await generateLink(url, "getsitecopy");
      }
    }
  }

  const isValidURL = function (url) {
    const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(url);
  };

  if (isValidURL(url)) {
    document.getElementById("generated-url-container").classList.add("d-none");
    return generateURL(url).then(mirrorURL => {
      if (mirrorURL) {
        // Copy URL to clipboard
        urlOutput.textContent = mirrorURL;
        document.getElementById("generated-url-container").classList.remove("d-none");
        errorMessage.textContent = "Copied link to clipboard / Ссылка скопирована в буфер обмена";
        return copyTextToClipboard(mirrorURL);
      } else {
        errorMessage.textContent = "Unable to build mirror URL / Не удалось создать URL-адрес зеркала";
        document.getElementById("generated-url-container").classList.add("d-none");
        return false;
      }
    });
  } else {
    errorMessage.textContent = "Invalid URL / Неверный Ввод";
    document.getElementById("generated-url-container").classList.add("d-none");
    return false;
  }
}

urlInput.addEventListener('change', function () {
  console.log('Change')
  executeRoutine(urlInput.value);
});

document.getElementById('url-form').addEventListener('submit', function (e) {
  e.preventDefault();
  console.log('Submit')
  executeRoutine(urlInput.value);

});

urlInput.addEventListener('paste', function () {
  console.log('Paste')
  let paste = (event.clipboardData || window.clipboardData).getData('text');
  executeRoutine(paste);
});

urlOutput.addEventListener('click', function () {
  copyTextToClipboard(urlOutput.innerText);
});
