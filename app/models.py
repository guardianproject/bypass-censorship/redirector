from datetime import datetime

from app.extensions import db


class Link(db.Model):  # type: ignore[name-defined,misc]
    """
    Represents a short link in the database.

    :param id: The primary key of the link.
    :param pool: The pool to which the link belongs.
    :param origin_domain_name: The domain name of the origin server.
    :param url_path: The path of the URL associated with the short link.
    """

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    link_type = db.Column(db.String())
    pool = db.Column(db.String(), nullable=True)
    origin_domain_name = db.Column(db.String())
    url_path = db.Column(db.String())
    created_at = db.Column(db.DateTime(), default=datetime.utcnow)
    refreshed_at = db.Column(db.DateTime(), nullable=True)
    expiry_days = db.Column(db.Integer())
    expires_at = db.Column(db.DateTime())
    footer_text = db.Column(db.String(), nullable=True)
    footer_link = db.Column(db.String(), nullable=True)
    deletion_requested = db.Column(db.DateTime(), nullable=True)
    deletion_confirmed = db.Column(db.DateTime(), nullable=True)


class SnapshotImage(db.Model):  # type: ignore[name-defined,misc]
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    image_hash = db.Column(db.String())
    expires_at = db.Column(db.DateTime())
    deletion_confirmed = db.Column(db.DateTime(), nullable=True)


class Blocklist(db.Model):  # type: ignore[name-defined,misc]
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    origin_domain_name = db.Column(db.String())
    url_path = db.Column(db.String())
    created_at = db.Column(db.DateTime(), default=datetime.utcnow)


class UserReport(db.Model):  # type: ignore[name-defined,misc]
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    origin_domain_name = db.Column(db.String())
    url_path = db.Column(db.String())
    count = db.Column(db.Integer())
    created_at = db.Column(db.DateTime(), default=datetime.utcnow)
    last_reported_at = db.Column(db.DateTime())
