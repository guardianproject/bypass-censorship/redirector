import logging
import os.path

import yaml
from flask import Flask
from prometheus_client import disable_created_metrics

from app.extensions import db, migrate, geoip_reader, hashids, scheduler
from app.pool_data import load_initial_data
from app.redirector import redirector

app = Flask(__name__)

disable_created_metrics()  # type: ignore[no-untyped-call]

logging.basicConfig()
app.config.from_file("../config.yaml", load=yaml.safe_load)

app.config["MIRROR_COUNTRIES"] = [country.upper() for country in app.config.get("MIRROR_COUNTRIES", [])]

db.init_app(app)
migrate.init_app(app, db)
hashids.init_app(app)
geoip_reader.init_app(app)
scheduler.init_app(app)

app.register_blueprint(redirector)

if os.path.exists("data.json"):
    load_initial_data()

if not app.debug or os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
    scheduler.start()

if __name__ == '__main__':
    app.run()
