import datetime
import json
import logging
from typing import Optional, TYPE_CHECKING, Tuple, List
from urllib.parse import urlparse, parse_qsl, urlencode

import geoip2.errors
import requests
from flask import current_app, jsonify, request, render_template, Response, redirect, Blueprint
from flask.typing import ResponseValue
from prometheus_client import generate_latest
from sqlalchemy import desc

from app.extensions import db, geoip_reader, hashids, google_client
from app.gsc import parse, DomainNotSupportedWarning, UnparsableArticleWarning
from app.metrics import links_created, links_requested, short_link_visits, user_reports, google_storage_errors
from app.models import Link, Blocklist, UserReport, SnapshotImage
from app.pool_data import Pool, lookup_pool, lookup_mirror, update_data
from app.static import get_opengraph_tags, blob_to_bucket, delete_from_bucket
from app.util import not_found, authenticated_view, admin_view

redirector = Blueprint("redirector", __name__)


@redirector.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@redirector.app_template_filter('hashid')
def filter_hashid(id_: int) -> str:
    return hashids.encode(id_)


def generate_link_live(pool: Pool, url: str, type_: str) -> ResponseValue:
    """
    Generates a link based on the provided type.

    The function will first parse the URL and check if the domain is found in the pool's origins.
    If not, it will try again with the public pool. If still it cannot be found, it will return a
    404 response. Otherwise, it will generate a direct or short link based on the type provided.

    :param pool: The pool of resources where the domain should be found.
    :param url: The URL that needs to be converted into a direct or short link.
    :param type_: The type of link to be generated. This can be "direct" or "short".
    :returns: A JSON response containing the generated link.
    :raises NotImplementedError: If the type is not "direct" or "short".
    """
    if type_ not in ["live-direct", "live-short", "live-robust"]:
        raise NotImplementedError("generate_link_live")
    public_pool = lookup_pool(current_app.config["PUBLIC_KEY"])
    if not public_pool:
        return not_found()
    domain_name, path = parse_url(url)
    if not lookup_mirror(pool, domain_name):
        pool = public_pool
        if not lookup_mirror(pool, domain_name):
            return not_found()
    links_requested.labels(
        source_domain=domain_name,
        pool=pool['short_name'],
        type=type_
    ).inc()
    if type_ == "live-direct":
        return jsonify({
            "url": generate_link_live_direct(pool, domain_name, path)
        })
    # It's a short or static link, because we narrowed down what it could be in the very
    # first if statement of this function.
    return jsonify({
        "url": generate_link_live_redirector(pool, domain_name, path, type_ == "live-robust")
    })


def generate_link_live_direct(pool: Pool, domain_name: str, path: str) -> Optional[str]:
    """
    Generates a link directly to a mirror.

    :param pool: Resource pool of mirror.
    :param domain_name: Domain name of mirror.
    :param path: The URL path and query to append to mirror domain to form the full mirror URL.
    :return: The full mirror URL including the path and query string.
    """
    mirror = lookup_mirror(pool, domain_name)
    if not mirror:
        return None
    return f"{mirror}{path}"


def generate_link_live_redirector(pool: Pool, domain_name: str, path: str, static: bool = False) -> str:
    """
    Generates a short link that redirects to a mirror.

    :param pool: Resource pool of mirror.
    :param domain_name: Domain name of mirror.
    :param path: The URL path and query to append to mirror domain to form the full mirror URL.
    :param static: Whether to return a static link instead of short link.
    :return: The full mirror URL including the path and query string.
    """
    pool_name = pool["short_name"]
    link: Optional[Link] = Link.query.filter(Link.pool == pool_name,
                                             Link.origin_domain_name == domain_name,
                                             Link.url_path == path).first()
    if link is None:
        link = Link(pool=pool_name,
                    origin_domain_name=domain_name,
                    url_path=path,
                    created_at=datetime.datetime.utcnow(),
                    expires_at=datetime.datetime.utcnow() + datetime.timedelta(days=30),
                    link_type="live-robust" if static else "live-short")
        db.session.add(link)
        links_created.labels(
            source_domain=domain_name,
            pool=pool_name,
            type="live-robust" if static else "live-short"
        ).inc()
        try:
            refresh_static_link(link)
            link.refreshed_at = datetime.datetime.utcnow()
        except Exception as exc:
            logging.exception(exc)
        db.session.commit()
    if static:
        return (f"https://storage.googleapis.com/{current_app.config['GOOGLE_STORAGE_BUCKET']}"
                f"/{hashids.encode(link.id)}.html")
    else:
        return (f"https://{pool['redirector_domain'] or current_app.config['DEFAULT_REDIRECTOR_DOMAIN']}"
                f"/{hashids.encode(link.id)}")


def generate_link_snapshot(pool: Pool,
                           url: str,
                           expiry_days: int,
                           footer_text: Optional[str],
                           footer_link: Optional[str],
                           force_new: bool = False) -> str:
    domain_name, path = parse_url(url)
    block = Blocklist.query.filter(
        Blocklist.origin_domain_name == domain_name,
        Blocklist.url_path == path
    ).first()
    if block:
        raise UnparsableArticleWarning
    try:
        pool_name = pool["short_name"]
    except TypeError:
        pool_name = "public"
    links_requested.labels(source_domain=domain_name,
                           pool=pool_name,
                           type="snapshot").inc()
    link: Optional[Link] = Link.query.filter(Link.pool == pool_name,
                                             Link.origin_domain_name == domain_name,
                                             Link.url_path == path,
                                             Link.expiry_days == expiry_days,
                                             Link.footer_text == footer_text,
                                             Link.footer_link == footer_link).first()
    if link is None or force_new:
        link = Link(pool=pool_name, origin_domain_name=domain_name, url_path=path,
                    created_at=datetime.datetime.utcnow(),
                    expiry_days=expiry_days,
                    expires_at=datetime.datetime.utcnow() + datetime.timedelta(days=expiry_days),
                    footer_text=footer_text, footer_link=footer_link,
                    link_type="snapshot")
        db.session.add(link)
        db.session.commit()
        try:
            refresh_snapshot_link(link,
                                  generate_link_live_direct(pool, domain_name, "/"),
                                  generate_link_live_direct(pool, domain_name, path))
            links_created.labels(source_domain=domain_name,
                                 pool=pool_name,
                                 type="snapshot").inc()
            return f"https://storage.googleapis.com/{current_app.config['GOOGLE_STORAGE_BUCKET']}/{hashids.encode(link.id)}.html"
        except (DomainNotSupportedWarning, UnparsableArticleWarning) as exc:
            logging.exception(exc)
            db.session.delete(link)
            db.session.commit()
            raise exc
    elif pool['api_key'] != current_app.config['PUBLIC_KEY']:
        try:
            head = requests.head(url, timeout=6.1)
        except requests.exceptions.Timeout:
            logging.error("Timeout waiting for HEAD request on source article")
            raise UnparsableArticleWarning
        if head.status_code != 200:
            link.deletion_requested = datetime.datetime.utcnow()
            if delete_from_bucket(
                    google_client,
                    current_app.config['GOOGLE_STORAGE_BUCKET'],
                    f"{hashids.encode(link.id)}.html"
            ):
                link.deletion_confirmed = datetime.datetime.utcnow()
            else:
                google_storage_errors.labels(
                    upload_type="delete-self-service"
                ).inc()
            db.session.commit()
            logging.error("Source article returned non-200 status code")
            raise UnparsableArticleWarning
    link.expires_at = datetime.datetime.utcnow() + datetime.timedelta(days=expiry_days)
    db.session.commit()
    return f"https://storage.googleapis.com/{current_app.config['GOOGLE_STORAGE_BUCKET']}/{hashids.encode(link.id)}.html"


def client_needs_mirror(client_ip: Optional[str]) -> bool:
    """
    Check if a client needs to be redirected to a mirror server based on their country.

    If the client IP is None, this function returns True.

    If no GeoIP database was provided in the configuration file, this function always
    returns True.

    If the IP address does not have a country associated in the database, this function
    returns True.

    :param client_ip: The IP address of the client.
    :return: True if the client needs to be redirected to a mirror server, False otherwise.
    """
    logging.debug(f"client_needs_mirror called with client_ip: {client_ip}")

    if client_ip is None or geoip_reader is None:
        logging.debug("client_ip or geoip_reader is None. Returning True.")
        return True

    try:
        result = geoip_reader.country(client_ip)
        logging.debug(f"GeoIP reader country result: {result}")
    except geoip2.errors.AddressNotFoundError:
        logging.debug("Address not found in GeoIP reader. Returning True.")
        return True

    if result.country.iso_code is None:
        logging.debug("ISO code is None. Returning True.")
        return True

    if TYPE_CHECKING:
        assert isinstance(result.country.iso_code, str)  # nosec: B101
        logging.debug("TYPE_CHECKING is True, ISO code is string.")

    mirror_countries = current_app.config['MIRROR_COUNTRIES']
    logging.debug(f"Mirror Countries: {mirror_countries}")

    needs_mirror = result.country.iso_code.upper() in mirror_countries
    logging.debug(f"Does client need mirror? {needs_mirror}")

    return needs_mirror


def parse_url(url: str) -> Tuple[str, str]:
    """
    Parses the provided URL into domain name and path.

    .. note:: The `force` and `force_ip` query parameters have special meaning in this application
              and will be removed from the result.

    :param url: The URL that needs to be parsed.
    :return: A tuple containing the domain name and path from the parsed URL.
    """
    parsed_url = urlparse(url)
    domain_name = parsed_url.netloc.split(":")[0].lower()

    # Extract query parameters and filter out "force" and "force_ip"
    query_params = parse_qsl(parsed_url.query)
    filtered_query_params = [(key, value) for key, value in query_params if key.lower() not in ["force", "force_ip"]]

    # Reconstruct the path and query string
    path = parsed_url.path or '/'
    query_string = urlencode(filtered_query_params)

    # Combine the path and query string
    path_with_query = path if not query_string else f"{path}?{query_string}"

    return domain_name, path_with_query


@redirector.route('/me')
@authenticated_view
def route_me(pool: Pool, anonymous: bool) -> ResponseValue:
    return jsonify({
        "anonymous": anonymous,
        "description": pool["description"]
    })


@redirector.route("/bldr", methods=['GET', 'POST'])
@authenticated_view
def route_link_builder_v2(pool: Pool, anonymous: bool) -> ResponseValue:
    url: Optional[str] = request.args.get('url', None)
    generated_url: Optional[str] = None
    if url:
        parsed_url = parse_url(url)
        if anonymous:
            generated_url = generate_link_live_direct(pool, *parsed_url)
        else:
            generated_url = generate_link_live_redirector(pool, *parsed_url)
    return render_template("form.html.j2",
                           key=request.args.get('key', None),
                           url=url,
                           generated_url=generated_url)


@redirector.route("/admn", methods=['GET', 'POST'])
@admin_view
def route_admin() -> ResponseValue:
    return render_template("admin.html.j2")


@redirector.route("/tkdn", methods=['GET', 'POST'])
@admin_view
def route_takedown() -> ResponseValue:
    return render_template("takedown.html.j2",
                           back_link="/admn")


@redirector.route("/tkd1", methods=['GET', 'POST'])
@admin_view
def route_takedown_process() -> ResponseValue:
    if request.method == "GET":
        return render_template("takedown-start.html.j2", back_link="/tkdn", validation_error=False)
    step = request.form.get("step")
    if step == "source-article-url-known":
        if request.form.get("sourceArticleURLKnown") == "yes":
            return render_template("takedown-source.html.j2",
                                   back_link="/tkd1")
        elif request.form.get("sourceArticleURLKnown") == "no":
            return render_template("takedown-snapshot.html.j2",
                                   back_link="/tkd1",
                                   validation_error=False)
        else:
            return render_template("takedown-start.html.j2", validation_error=True)
    source_url = None
    if step == "snapshot-article-url":
        try:
            hash_ = request.form.get("snapshot-url", "").split("/")[-1].split(".")[0]
            link = Link.query.filter(Link.id == hashids.decode(hash_)[0]).first()
            if not link:
                return render_template("takedown-snapshot.html.j2",
                                       back_link="/tkd1",
                                       validation_error="not_found")
            source_url = f"https://{link.origin_domain_name}{link.url_path}"
        except IndexError:
            return render_template("takedown-snapshot.html.j2",
                                   back_link="/tkd1",
                                   validation_error="parse")
    if step in ["source-article-url", "summary"]:
        source_url = request.form.get("source-url", "").strip()
    if step in ['source-article-url', 'snapshot-article-url', 'summary']:
        if not source_url:
            return Response("Error", status=500)
        domain_name, path = parse_url(source_url)
        results = Link.query.filter(
            Link.origin_domain_name == domain_name,
            Link.url_path == path,
            Link.link_type == "snapshot"
        ).all()
        if not results:
            return render_template("takedown-source.html.j2",
                                   back_link="/tkd1",
                                   validation_error="not_found")
    if step in ['source-article-url', 'snapshot-article-url']:
        return render_template("takedown-confirm.html.j2",
                               source_url=source_url,
                               query_results=results,
                               storage_bucket=current_app.config['GOOGLE_STORAGE_BUCKET'],
                               back_link="/tkd1")
    if step == 'summary':
        total_snapshots = 0
        takedown_success = 0
        block = Blocklist.query.filter(
            Blocklist.origin_domain_name == domain_name,
            Blocklist.url_path == path
        ).first()
        if block is None:
            block = Blocklist(
                origin_domain_name=domain_name,
                url_path=path,
                created_at=datetime.datetime.utcnow()
            )
            db.session.add(block)
        for result in results:
            total_snapshots += 1
            if result.deletion_requested is None:
                result.deletion_requested = datetime.datetime.utcnow()
            if result.deletion_confirmed is None:
                if delete_from_bucket(
                        google_client,
                        current_app.config['GOOGLE_STORAGE_BUCKET'],
                        f"{hashids.encode(result.id)}.html"
                ):
                    takedown_success += 1
                    result.deletion_confirmed = datetime.datetime.utcnow()
                else:
                    google_storage_errors.labels(
                        upload_type="delete-admin"
                    ).inc()
            else:
                takedown_success += 1
        db.session.commit()
        return render_template("takedown-finish.html.j2",
                               query_results=results,
                               total_snapshots=total_snapshots,
                               source_url=source_url,
                               takedown_success=takedown_success,
                               storage_bucket=current_app.config['GOOGLE_STORAGE_BUCKET'],
                               now=datetime.datetime.utcnow())
    return "Error"


@redirector.route("/sqry")
@admin_view
def route_snapshot_query() -> ResponseValue:
    return render_template("query.html.j2")


@redirector.route("/sqr1")
@admin_view
def route_snapshot_query_process() -> ResponseValue:
    if request.args.get('step') == "url-form":
        source_known = request.args.get('sourceArticleURLKnown')
        if source_known == "yes":
            return render_template("query-source.html.j2",
                                   back_link="/sqr1")
        elif source_known == "no":
            return render_template("query-snapshot.html.j2",
                                   back_link="/sqr1")
        else:
            return render_template("query-start.html.j2",
                                   back_link="/sqry",
                                   validation_errors=True)
    source_url = None
    results = None
    if request.args.get('step') == "snapshot-url":
        try:
            hash_ = request.args.get("snapshot-url", "").split("/")[-1].split(".")[0]
            link = Link.query.filter(Link.id == hashids.decode(hash_)[0]).first()
            if not link:
                return render_template("query-snapshot.html.j2",
                                       back_link="/sqr1",
                                       validation_error="not_found")
            source_url = f"https://{link.origin_domain_name}{link.url_path}"
        except IndexError:
            return render_template("query-snapshot.html.j2",
                                   back_link="/sqr1",
                                   validation_error="parse")
    if request.args.get('step') == "source-url" or request.args.get("source-url"):
        source_url = request.args.get("source-url")
    if source_url:
        domain_name, path = parse_url(source_url)
        results = Link.query.filter(
            Link.origin_domain_name == domain_name,
            Link.url_path == path,
            Link.link_type == "snapshot"
        ).all()
    if request.args.get('step') == "source-url" or request.args.get("source-url"):
        if not results:
            return render_template("query-source.html.j2",
                                   back_link="/sqr1",
                                   validation_error="not_found")
    if results:
        return render_template("query-results.html.j2",
                               source_url=source_url,
                               query_results=results,
                               storage_bucket=current_app.config['GOOGLE_STORAGE_BUCKET'],
                               back_link="/sqr1"
                               )
    else:
        return render_template("query-start.html.j2",
                               back_link="/admn")


@redirector.route('/rprt', methods=['POST'])
def route_user_report() -> Tuple[ResponseValue, int]:
    if not request.json:
        return jsonify({"error": "missing json"}), 400
    bad_url = request.json.get('bad_url')
    if not bad_url:
        return jsonify({"error": "no bad_url"}), 400
    try:
        origin_domain_name, url_path = parse_url(bad_url)
    except (ValueError, IndexError):
        return jsonify({"error": "bad bad_url"}), 400
    user_reports.labels(
        source_domain=origin_domain_name
    ).inc()
    report = UserReport.query.filter(
        UserReport.origin_domain_name == origin_domain_name,
        UserReport.url_path == url_path
    ).first()
    if not report:
        report = UserReport(
            origin_domain_name=origin_domain_name,
            url_path=url_path,
            count=0,
            created_at=datetime.datetime.utcnow()
        )
        db.session.add(report)
    report.last_reported_at = datetime.datetime.utcnow()
    report.count += 1
    db.session.commit()
    return jsonify({"success": True}), 200


@redirector.route('/urpt')
@admin_view
def route_user_reports() -> ResponseValue:
    reports = UserReport.query.order_by(desc(UserReport.last_reported_at)).limit(50).all()
    for report in reports:
        block = Blocklist.query.filter(
            Blocklist.origin_domain_name == report.origin_domain_name,
            Blocklist.url_path == report.url_path
        ).first()
        if block:
            report.blocklist = True
        else:
            report.blocklist = False
    return render_template("reports.html.j2",
                           reports=reports,
                           back_link="/admn")


@redirector.route('/link')
@authenticated_view
def route_link(pool: Pool, anonymous: bool) -> ResponseValue:
    url = request.args['url']
    type_ = request.args.get('type', 'direct')
    if type_ in ["getsitecopy", "snapshot"]:
        # TODO: Deprecate "getsitecopy"
        if anonymous:
            expiry_days = 30
            footer_text = None
            footer_link = None
        else:
            expiry_days = 30 if request.args.get('expiry', 'short') == 'short' else 90
            footer_text = request.args.get('footer_text', None)
            footer_link = request.args.get('footer_link', None)
        force_new = current_app.debug and request.args.get('force_new') == "true"
        try:
            return jsonify({
                'url': generate_link_snapshot(pool,
                                              url,
                                              expiry_days=expiry_days,
                                              footer_text=footer_text,
                                              footer_link=footer_link,
                                              force_new=force_new)
            })
        except DomainNotSupportedWarning:
            return Response("The domain is not supported", status=403)
        except UnparsableArticleWarning:
            return Response("The article could not be parsed", status=500)
    if type_ == "live":
        type_ = "live-short"
    if anonymous and type_ in ['short', 'live-short']:
        # TODO: Deprecate "short"
        type_ = "live-robust"
    # TODO: Deprecate these aliases
    if type_ == "direct":
        type_ = "live-direct"
    if type_ == "short":
        type_ = "live-short"
    if type_ == "static":
        type_ = "live-robust"
    return generate_link_live(pool, url, type_)


@redirector.route('/updt', methods=['POST'])
@admin_view
def route_update() -> ResponseValue:
    update_data(request.json)
    with open('data.json', 'w') as data_file:
        data_file.write(json.dumps(request.json))
    return jsonify({"success": True})


@redirector.route("/")
def route_homepage() -> ResponseValue:
    """
    Render the homepage, which is a blank page with a 200 status.

    :return: The HTTP response object for the homepage.
    """
    return Response("", 200)


def generate_link_original(origin_domain_name: str, url_path: str) -> str:
    """
    Generate the original link using the origin domain name and URL path.

    :param origin_domain_name: The domain name of the origin server.
    :param url_path: The path of the URL.
    :return: The generated original link.
    """
    return f"https://{origin_domain_name}{url_path}"


@redirector.route("/mtrc")
@admin_view
def route_metrics() -> ResponseValue:
    return Response(generate_latest(), headers={"Content-Type": "text/plain"})


@redirector.route('/<hash_>')
def route_short_link(hash_: str) -> ResponseValue:
    logging.debug(f"Route short link called with hash: {hash_}")

    if hash_ in current_app.config['SIMPLE_OVERRIDES']:
        return redirect(current_app.config['SIMPLE_OVERRIDES'][hash_], 302)

    # This has to go last due to the matching rules
    decoded_hash = hashids.decode(hash_)
    logging.debug(f"Decoded hash: {decoded_hash}")

    if not decoded_hash:
        logging.debug("No decoded hash found. Responding with 200 status.")
        return Response("", status=200)

    link = Link.query.filter(Link.id == decoded_hash[0]).first()
    logging.debug(f"Link query result: {link}")

    if link is None:
        logging.debug("Link not found. Responding with 200 status.")
        return Response("", status=200)

    short_link_visits.labels(
        source_domain=link.origin_domain_name,
        pool=link.pool
    ).inc()

    pool = lookup_pool(name=link.pool)
    logging.debug(f"Pool lookup result: {pool}")

    if pool is None:
        logging.debug("Pool not found.")
        return not_found()

    # Check if "force_ip" parameter exists in query parameters, otherwise use the
    # "X-Forwarded-For" header, or None.
    client_ip = request.args.get("force_ip",
                                 request.headers.get("X-Forwarded-For", None))
    logging.debug(f"Client IP: {client_ip}")

    if request.args.get("force") or client_needs_mirror(client_ip):
        target = generate_link_live_direct(
            pool, link.origin_domain_name, link.url_path)
    else:
        target = generate_link_original(
            link.origin_domain_name, link.url_path)

    logging.debug(f"Target: {target}")
    return redirect(target)


def refresh_static_link(link: Link) -> None:
    tags: Optional[List[Tuple[str, str]]] = get_opengraph_tags(f"https://{link.origin_domain_name}{link.url_path}")
    if tags is None:
        return  # We'll just try again next time
    pool = lookup_pool(None, link.pool)
    if pool is None:
        logging.error("No such pool could be found.")
        return
    rendered = render_template("static.html.j2", opengraph=tags, direct_url=generate_link_live_direct(
        pool, link.origin_domain_name, link.url_path
    ))
    if blob_to_bucket(google_client, current_app.config['GOOGLE_STORAGE_BUCKET'], rendered, None,
                      f"{hashids.encode(link.id)}.html"):
        link.refreshed_at = datetime.datetime.utcnow()
        db.session.commit()
    else:
        google_storage_errors.labels(
            upload_type="upload-robust"
        ).inc()


def refresh_snapshot_link(link: Link,
                          mirror_link_homepage: Optional[str] = None,
                          mirror_link_article: Optional[str] = None) -> None:
    html_bytes, images = parse(f"https://{link.origin_domain_name}{link.url_path}",
                               footer_text=link.footer_text,
                               footer_link=link.footer_link,
                               mirror_link_homepage=mirror_link_homepage,
                               mirror_link_article=mirror_link_article)
    url_name = f"{hashids.encode(link.id)}.html"
    if not blob_to_bucket(google_client,
                          current_app.config['GOOGLE_STORAGE_BUCKET'],
                          html_bytes.getvalue(),
                          None, url_name):
        google_storage_errors.labels(
            upload_type="upload-snapshot-html"
        ).inc()
        logging.error("Failed to upload HTML to Google Storage bucket")
        raise UnparsableArticleWarning
    for item in range(0, len(images)):
        needs_upload = False
        image_hash = images[item][2]
        image = SnapshotImage.query.filter(SnapshotImage.image_hash == image_hash).first()
        if not image:
            image = SnapshotImage(image_hash=image_hash, expires_at=link.expires_at)
            db.session.add(image)
            needs_upload = True
        if image.deletion_confirmed:
            needs_upload = True
        if needs_upload:
            if not blob_to_bucket(google_client, current_app.config['GOOGLE_STORAGE_BUCKET'],
                                  images[item][0].getvalue(),
                                  images[item][1],
                                  f"images/{image_hash}"):
                google_storage_errors.labels(
                    upload_type="upload-snapshot-image"
                ).inc()
                logging.error("Failed to upload image to Google Storage bucket")
                raise UnparsableArticleWarning
        if image.expires_at < link.expires_at:
            image.expires_at = link.expires_at
        image.deletion_confirmed = None
    link.refreshed_at = datetime.datetime.utcnow()
    db.session.commit()
