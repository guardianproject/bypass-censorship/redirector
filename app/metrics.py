from prometheus_client import Counter

links_created = Counter("links_created_total",
                        "Number of links created",
                        ["type", "source_domain", "pool"])
links_requested = Counter("links_requested_total",
                          "Number of links requested",
                          ["type", "source_domain", "pool"])
short_link_visits = Counter("links_visits",
                            "Number of short link visits",
                            ["source_domain", "pool"])
user_reports = Counter("user_reports",
                       "Number of user reports received",
                       ["source_domain"])
google_storage_errors = Counter("google_storage_errors",
                                "Number of failed upload attempts to Google Storage",
                                ["upload_type"])
