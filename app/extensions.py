import os
from ipaddress import IPv4Address, IPv6Address
from typing import Optional, Any, Tuple, Union

from flask import Flask
from flask_apscheduler import APScheduler
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from geoip2.database import Reader
from geoip2.models import Country
from google.cloud import storage
from hashids import Hashids as HashidsE


class Hashids:
    hashids: Optional[HashidsE]

    def __init__(self) -> None:
        self.hashids = None

    def init_app(self, app: Flask) -> None:
        self.hashids = HashidsE(min_length=5, salt=app.config['SECRET_KEY'])

    def decode(self, hashid: str) -> Tuple[int, ...]:
        if self.hashids is None:
            raise RuntimeError("hashids wasn't initialised")
        return self.hashids.decode(hashid)  # type: ignore[no-any-return]

    def encode(self, *values: Any) -> str:
        if self.hashids is None:
            raise RuntimeError("hashids wasn't initialised")
        return self.hashids.encode(*values)  # type: ignore[no-any-return]


class GeoIPReader:
    geoip_reader: Optional[Reader]

    def __init__(self) -> None:
        self.geoip_reader = None

    def init_app(self, app: Flask) -> None:
        """
        Set up the GeoIP reader to retrieve country information.

        This function attempts to locate the GeoIP database file path from the application configuration.
        If the database file path is not provided in the configuration, it defaults to
        `/usr/share/GeoIP/GeoIP2-Country.mmdb`.

        If the GeoIP database file exists at the specified path, a GeoIP reader is created using the file and returned.
        Otherwise, if the file does not exist, `None` is returned.
        """
        geoip_path = app.config.get("GEOIP_DATABASE", "/usr/share/GeoIP/GeoIP2-Country.mmdb")
        if os.path.exists(geoip_path):
            self.geoip_reader = Reader(geoip_path)

    def is_initialised(self) -> bool:
        return self.geoip_reader is not None

    def country(self, ip_address: Union[str, IPv4Address, IPv6Address]) -> Country:
        if self.geoip_reader is None:
            raise RuntimeError("geoip reader wasn't initialised")
        return self.geoip_reader.country(ip_address)


db = SQLAlchemy()
migrate = Migrate()
hashids = Hashids()
geoip_reader = GeoIPReader()
google_client = storage.Client.from_service_account_json('google_service.json')
scheduler = APScheduler()
