import datetime
import logging

from flask import current_app
from sqlalchemy import select

from app.extensions import db, hashids, google_client, scheduler
from app.metrics import google_storage_errors
from app.models import Link, SnapshotImage
from app.pool_data import lookup_pool
from app.redirector import refresh_static_link, refresh_snapshot_link, generate_link_live_direct
from app.static import delete_from_bucket


@scheduler.task("interval", minutes=1)  # type: ignore[misc]
def batch_expire() -> None:
    with scheduler.app.app_context():
        logging.info("Deleting all expired snapshots and images from Google Storage bucket")
        stmt = select(Link).where(
            Link.link_type == "snapshot",
            Link.expires_at < datetime.datetime.utcnow(),
            Link.deletion_confirmed.is_(None)
        )
        result = db.session.execute(stmt)
        expired_snapshots = result.scalars().all()
        for expired_snapshot in expired_snapshots:
            if delete_from_bucket(
                    google_client,
                    current_app.config['GOOGLE_STORAGE_BUCKET'],
                    f"{hashids.encode(expired_snapshot.id)}.html"
            ):
                expired_snapshot.deletion_confirmed = datetime.datetime.utcnow()
            else:
                google_storage_errors.labels(
                    upload_type="delete-expired-snapshot"
                ).inc()
        stmt = select(SnapshotImage).where(
            SnapshotImage.expires_at < datetime.datetime.utcnow(),
            SnapshotImage.deletion_confirmed.is_(None)
        )
        result = db.session.execute(stmt)
        expired_images = result.scalars().all()
        for expired_image in expired_images:
            if delete_from_bucket(
                    google_client,
                    current_app.config['GOOGLE_STORAGE_BUCKET'],
                    f"images/{expired_image.image_hash}"
            ):
                expired_image.deletion_confirmed = datetime.datetime.utcnow()
            else:
                google_storage_errors.labels(
                    upload_type="delete-expired-image"
                ).inc()
        db.session.commit()


@scheduler.task("interval", minutes=1)  # type: ignore[misc]
def batch_refresh() -> None:
    with scheduler.app.app_context():
        logging.info("Refreshing all non-expired snapshots more than 30 minutes old")
        stmt = select(Link).where(
            Link.link_type == "snapshot",
            Link.expires_at > datetime.datetime.utcnow(),
            Link.refreshed_at < datetime.datetime.utcnow() - datetime.timedelta(minutes=30),
            Link.deletion_requested.is_(None)
        )
        result = db.session.execute(stmt)
        snapshots = result.scalars().all()
        for link in snapshots:
            pool = lookup_pool(name=link.pool)
            if not pool:
                pool = lookup_pool(current_app.config["PUBLIC_KEY"])
            if pool:
                refresh_snapshot_link(link,
                                      generate_link_live_direct(pool, link.origin_domain_name, "/"),
                                      generate_link_live_direct(pool, link.origin_domain_name, link.url_path))
            else:
                refresh_snapshot_link(link, None, None)
        logging.info("Refreshing all non-expired robust links more than 30 minutes old")
        stmt = select(Link).where(
            Link.link_type == "live-robust",
            Link.expires_at > datetime.datetime.utcnow(),
            Link.refreshed_at < datetime.datetime.utcnow() - datetime.timedelta(minutes=30),
            Link.deletion_requested.is_(None)
        )
        result = db.session.execute(stmt)
        robust_links = result.scalars().all()
        for link in robust_links:
            refresh_static_link(link)
