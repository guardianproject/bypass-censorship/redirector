import copy
import hashlib
import logging
import re
import unicodedata
from io import BytesIO
from typing import Any, Callable, Dict, List, Tuple, Optional, Union

import jinja2
# It would be nice if defusedxml modules existed for HTML handling, but they don't appear to currently
import lxml  # nosec: B410
import requests
import tldextract
import yaml
from PIL import Image
from babel.support import Translations
from bs4 import BeautifulSoup
from flask import current_app
from jinja2 import Environment, PackageLoader, select_autoescape
from lxml.html import HTMLParser, Element  # nosec: B410
from lxml.html.clean import Cleaner as HTMLCleaner  # nosec: B410
from requests.auth import HTTPBasicAuth

def normalize(text: str) -> str:
    """Removes whitespace and normalizes Unicode."""
    return unicodedata.normalize("NFKD", re.sub(r"\s\s+", " ", text.strip()))


KeyFunction = Callable[[Any], Any]
XPathRule = Dict[str, Any]
XPathRules = List[XPathRule]


class DomainNotSupportedWarning(RuntimeWarning):
    pass


class UnparsableArticleWarning(RuntimeWarning):
    pass


def calculate_hash(buffer: BytesIO) -> str:
    buffer.seek(0)
    hash_obj = hashlib.sha256()
    buffer_size = 65536
    while True:
        chunk = buffer.read(buffer_size)
        if not chunk:
            break
        hash_obj.update(chunk)
    return hash_obj.hexdigest()


def parse(url: str,
          footer_text: Optional[str] = None,
          footer_link: Optional[str] = None,
          mirror_link_homepage: Optional[str] = None,
          mirror_link_article: Optional[str] = None
          ) -> Tuple[BytesIO, List[Tuple[BytesIO, str, str]]]:
    domain = tldextract.extract(url).registered_domain
    parser_config = None
    for parser_data in current_app.config['PARSERS']:
        for parser, data in parser_data.items():
            domains = data.get('domains', [])
            if domain in domains:
                parser_config = data.copy()
                break
    if parser_config is None:
        raise DomainNotSupportedWarning
    headers = parser_config['headers'] if 'headers' in parser_config else None
    username = parser_config['username'] if 'username' in parser_config else None
    password = parser_config['password'] if 'password' in parser_config else None
    if 'url_substitutions' in parser_config:
        for item in parser_config['url_substitutions']:
            url = url.replace(item, parser_config['url_substitutions'][item])
    parser_config['footer_text_override'] = footer_text
    parser_config['footer_link_override'] = footer_link
    parser_config['mirror_link_homepage'] = mirror_link_homepage
    parser_config['mirror_link_article'] = mirror_link_article
    content = requests.get(url,
                           headers=headers,
                           auth=HTTPBasicAuth(username, password),
                           timeout=6.1).content.decode('utf-8')
    parser = Parser(WebPage(url, content), **parser_config)
    return parser.result()  # type: ignore[no-any-return]


class WebPage:
    def __init__(self, url: str, content: str) -> None:
        self.url = url
        self.soup = BeautifulSoup(content, "html.parser")

    def hostname(self) -> str:
        return tldextract.extract(self.url).registered_domain

    def html(self, parser: lxml.html.HTMLParser = None, encoding: str = "utf-8") -> Any:
        return lxml.html.fromstring(
            html=self.soup.prettify(encoding=encoding),
            parser=parser,
        )


class Parser:
    """Article content parser.

    title_xpath: XPath expression for the title of the article.
    article_xpath: XPath expression for the article content.
    timestamp_xpath: XPath expression for the article timestamp.
    remove_on_xpath_match: List of tuples of (tag, regex) for tags to exclude from article.

    See https://devhints.io/xpath for more info about xpath.
    """

    # Template options
    amp: bool = False
    custom_js: List[str] = []
    custom_styles: List[str] = []
    favicon_path: Optional[str] = None
    footer_link: Optional[str] = None
    footer_link_override: Optional[str] = None
    footer_text: Optional[str] = None
    footer_text_override: Optional[str] = None
    google_analytics_tracking_id: Optional[str] = None
    homepage_link: Optional[str] = None
    logo_path: Optional[str] = None
    mirror_link_article: Optional[str] = None
    mirror_link_homepage: Optional[str] = None
    page_template: str = "article-template.html.j2"
    page_footer_template: Optional[str] = None
    page_header_template: Optional[str] = None
    remove_default_css: bool = False
    tealium: bool = False
    twitter_js: bool = False

    # Fetch options
    headers: Dict[str, str] = {}
    password: Optional[str] = None
    url_substitutions: Optional[Dict[str, str]] = None
    username: Optional[str] = None

    # Parser options
    article_image_xpath: Optional[str] = None
    article_image_caption_xpath: Optional[str] = None
    article_xpath: Optional[str] = None
    author_xpath: Optional[str] = None
    clean_on_xpath_match: XPathRules = []
    copyright_xpath: Optional[str] = None
    custom_logo_xpath: Optional[str] = None
    description_xpath: Optional[str] = None
    encoding: str = "utf-8"
    og_image_path: Optional[str] = None
    remove_on_xpath_match: XPathRules = []
    search_engine_indexing: bool = True
    timestamp_xpath: Optional[str] = None
    title_xpath: str = "//title/text()"

    # Images discovered during parsing
    images: List[Tuple[BytesIO, str, str]] = []

    # Default HTML Parser:
    html_parser = HTMLParser(
        recover=True,
        remove_comments=True,
        remove_blank_text=True,
    )
    # Default HTML Cleaner:
    html_cleaner = HTMLCleaner(
        forms=False,
        scripts=False,
        embedded=False,
        annoying_tags=False,
        remove_unknown_tags=False,
        safe_attrs_only=False,
    )

    jinja_env: Optional[jinja2.Environment] = None
    translations: Optional[Translations] = None

    def __init__(self, web_page: WebPage, **kwargs: Any) -> None:
        for key, value in kwargs.items():
            setattr(self, key, value)
        self.page = web_page
        self.html = self.page.html(
            parser=self.html_parser,
            encoding=self.encoding,
        )
        self.jinja_env = Environment(
            loader=PackageLoader(
                package_name="app.extensions",
                package_path="templates",
            ),
            autoescape=select_autoescape(),
            extensions=[
                'jinja2.ext.i18n',
            ],
            trim_blocks=True,
            lstrip_blocks=True,
        )

    def find(self, selector: str, key: Union[KeyFunction, None] = None) -> Element:
        """Finds the first element matching the selector."""
        elements = self.html.xpath(selector)
        if len(elements) < 1:
            raise ValueError(f"No element found: {selector}")
        element: Element = elements[0]
        if not key:
            return element
        return key(element)

    def get_meta(self, prop: str) -> Optional[str]:
        try:
            content: str = self.find(f"//meta[@property='{prop}']/@content")
            return content
        except ValueError:
            return None

    def get_og_image(self) -> Optional[str]:
        return self.get_meta("og:image") or self.get_meta("twitter:image") or self.og_image_path or None

    def get_article_image(self) -> Optional[str]:
        if self.article_image_xpath:
            src: str = self.find(self.article_image_xpath)
            if src and not src.startswith("data:"):  # If the image is base64 encoded, we don't need to upload it.
                if src.startswith("/"):  # If src is relative, we need to make it absolute.
                    src = f"https://{self.page.hostname()}{src}"
                resized_image = self._fetch_and_resize_image(src)
                self.images.append(resized_image)
                return f"images/{resized_image[2]}"
            return src
        return None

    def get_article_image_caption(self) -> Optional[str]:
        try:
            if not self.article_image_caption_xpath:
                return None
            caption: Optional[str] = self.find(self.article_image_caption_xpath, key=normalize)
            return caption
        except ValueError:
            return None

    def get_copyright(self) -> Optional[str]:
        try:
            if not self.copyright_xpath:
                return None
            copyright: Optional[str] = self.find(self.copyright_xpath, key=normalize)
            return copyright
        except ValueError:
            return None

    def get_author(self) -> Optional[str]:
        """Returns the author of the article."""
        if not self.author_xpath:
            return None
        try:
            result: str = self.find(self.author_xpath, key=normalize)
            return result
        except ValueError:
            return None


    def get_timestamp(self) -> Optional[str]:
        """Returns the timestamp of the article."""
        try:
            if not self.timestamp_xpath:
                return None
            result: str = self.find(self.timestamp_xpath, key=normalize)
            if result and "T" in result:
                return result.split("T")[0].strip()
            return result
        except ValueError:
            return None

    def get_title(self) -> Optional[str]:
        """Returns the title of the article."""
        if self.title_xpath:
            title: str = self.find(self.title_xpath, key=normalize)
            # TODO: Implement a parser config option to remove title text
            if 'moscowtimes.com' in self.page.hostname():
                title = title.replace(" - The Moscow Times", "")
            if 'moscowtimes.ru' in self.page.hostname():
                title = title.replace(" - Русская служба The Moscow Times", "")
            return title
        else:
            return None

    def get_description(self) -> Optional[str]:
        """Returns the description of the article."""
        if self.description_xpath is None:
            return None
        try:
            description: str = self.find(self.description_xpath, key=normalize)
            return description
        except ValueError:
            return None

    def get_text_direction(self) -> Optional[str]:
        direction: Optional[str] = self.find('//body').attrib.get("dir")
        if not direction:
            direction = self.find('//html').attrib.get("dir")
        return direction

    def get_language(self) -> Optional[str]:
        language: Optional[str] = self.find('//html').attrib.get("lang")
        return language

    def get_logo_path(self) -> Optional[str]:
        if self.logo_path:
            return self.logo_path
        if self.custom_logo_xpath:
            logo = self.find(self.custom_logo_xpath)
            src = logo.attrib.get("src")
            if src.startswith("/") and not src.startswith("//"):
                src = f"https://{self.page.hostname()}{src}"
            resized_image = self._fetch_and_resize_image(src)
            self.images.append(resized_image)
            return f"images/{resized_image[2]}"
        return None

    def _fetch_and_resize_image(self, url: str) -> Tuple[BytesIO, str, str]:
        if self.username and self.password:
            auth = HTTPBasicAuth(self.username, self.password)
        else:
            auth = None
        with requests.get(url, headers=self.headers, auth=auth, stream=True, timeout=6.1) as response:
            try:
                content_type = response.headers["Content-Type"]
            except KeyError:
                content_type = "image/jpeg"
            img_buf = BytesIO(response.content)
            if 'svg' in content_type:
                return img_buf, "image/svg+xml", calculate_hash(img_buf)
            img = Image.open(BytesIO(response.content))
            max_width = 650
            max_height = 650
            img.thumbnail((max_width, max_height))
            resized_img_buf = BytesIO()
            img.save(resized_img_buf, format=img.format)
            resized_img_buf.seek(0)
            return resized_img_buf, content_type, calculate_hash(resized_img_buf)

    def _find_images_and_replace(self, content: Element) -> List[Tuple[BytesIO, str, str]]:
        resized_images = []
        images = content.xpath(".//img[@src]|.//img[@data-src]|.//amp-img[@src]")
        ignored_attributes = (
            "height",
            "id",
            "sizes",
            "srcset",
            "styles",
            "width",
        )

        for image in images:
            src = image.attrib.get("src")
            if not src:
                data_src = image.attrib.get("data-src")
                if data_src:
                    src = data_src
            if image.tag != "amp-img":
                for attr in ignored_attributes:
                    image.attrib.pop(attr, None)
            if src and not src.startswith("data:"):  # If the image is base64 encoded, we don't need to upload it.
                if src.startswith("/"):  # If src is relative, we need to make it absolute.
                    src = f"https://{self.page.hostname()}{src}"
                # Resize the image and replace the src attribute.
                resized_image = self._fetch_and_resize_image(src)
                resized_images.append(resized_image)
                image.attrib.update({"src": f"images/{resized_image[2]}"})
        return resized_images

    def _remove_on_xpath_match(self, content: Any) -> None:
        for rule in self.remove_on_xpath_match:
            xpath = rule.get("xpath", None)
            namespaces = None
            if "re:" in xpath:
                namespaces = {
                    "re": "http://exslt.org/regular-expressions",
                }
            for tag in content.xpath(xpath, namespaces=namespaces):
                tag.drop_tree()

    # removes only the tag, keeps its children and text
    def _clean_on_xpath_match(self, content: Any) -> None:
        for rule in self.clean_on_xpath_match:
            xpath = rule.get("xpath", None)
            namespaces = None
            if "re:" in xpath:
                namespaces = {
                    "re": "http://exslt.org/regular-expressions",
                }
            for tag in content.xpath(xpath, namespaces=namespaces):
                tag.drop_tag()

    @staticmethod
    def _remove_tag_attributes(content: Element) -> None:
        content.attrib.clear()
        for element in content:
            element.attrib.clear()

    def _upload_links_and_replace(self, content: Element) -> None:
        links = content.xpath(".//a[@href]")
        for link in links:
            href = link.attrib.get("href")
            if self.mirror_link_homepage:
                if href.startswith("/") and len(href) > 1:
                    href = f"{self.mirror_link_homepage}{href[1:]}"
                href.replace(f"https://{self.page.hostname()}/", self.mirror_link_homepage)
                link.attrib.update({"href": href})
            else:
                if href.startswith("/"):
                    href = f"https://{self.page.hostname()}{href}"
                    link.attrib.update({"href": href})

    def get_content(self) -> str:
        """Returns the content of the article."""
        content = self.find(self.article_xpath)
        content = copy.deepcopy(content)  # make a deep copy before editing
        self.images.extend(self._find_images_and_replace(content))
        self._remove_on_xpath_match(content)
        self._clean_on_xpath_match(content)
        self._upload_links_and_replace(content)
        content = lxml.html.tostring(
            doc=content,
            pretty_print=True,
            encoding="unicode",
        )
        return normalize(self.html_cleaner.clean_html(content))

    def get_context(self) -> Dict[str, Any]:
        """Returns the Jinja-context of the article."""
        return {
            "amp": self.amp,
            "article_url": self.page.url,
            "title": self.get_title(),
            "content": self.get_content(),
            "copyright": self.get_copyright(),
            "source_url": self.page.url,
            "favicon": self.favicon_path,
            "author": self.get_author(),
            "datetime": self.get_timestamp(),
            "source_name": self.page.hostname(),
            "description": self.get_description(),
            "article_image": self.get_article_image(),
            "article_image_caption": self.get_article_image_caption(),
            "og_image": self.get_og_image(),
            "page_header_template": self.page_header_template,
            "page_footer_template": self.page_footer_template,
            "custom_styles": self.custom_styles,
            "custom_js": self.custom_js,
            "google_analytics_tracking_id": self.google_analytics_tracking_id,
            "remove_default_css": self.remove_default_css,
            "index": self.search_engine_indexing,
            "logo_path": self.get_logo_path(),
            "text_direction": self.get_text_direction(),
            "language": self.get_language(),
            "locale": self.get_language(),
            "footer_text": self.footer_text_override or self.footer_text or self.translations.gettext("View the original article"),
            "footer_link": self.footer_link_override or self.footer_link or self.mirror_link_article or None,
            "homepage_link": self.homepage_link or self.mirror_link_homepage or None,
            "matomo_url": current_app.config['MATOMO_URL'],
            "matomo_site_id": current_app.config['MATOMO_SITE_ID'],
            "tealium": self.tealium,
        }

    def result(self) -> Tuple[BytesIO, List[Tuple[BytesIO, str, str]]]:
        """Returns the article as a file."""
        try:
            self.translations = Translations.load("app/i18n", [self.get_language()])
            context = self.get_context()
        except Exception as exc:
            logging.exception(exc)
            raise UnparsableArticleWarning
        self.jinja_env.install_gettext_translations(self.translations)
        template = self.jinja_env.get_template(self.page_template)
        html = template.render(**context).encode("utf-8")
        return BytesIO(html), self.images


if __name__ == "__main__":
    class MockApp:
        config = None

    current_app = MockApp()  # type: ignore[assignment] # noqa: F811
    with open("../config.yaml", 'r') as file:
        current_app.config = yaml.safe_load(file)
    output = parse(
        "https://www.moscowtimes.ru/2024/05/05/vvashingtone-schitayut-chto-kiev-"
        "mozhet-nachat-novoe-kontrnastuplenie-v2025-godu-a129894")
    output[0].seek(0)
    print(output[0].read())
    print(output[1])
