import datetime
from functools import wraps
from typing import Callable, Any

from flask import Response, current_app, request, make_response
from flask.typing import ResponseValue

from app.pool_data import lookup_pool


def not_found() -> ResponseValue:
    """
    Generates a 404 error response.

    This function should be called when a request cannot be fulfilled due to the
    given parameters.

    :returns: A Response object with a 404 status code and an error message.
    """
    return Response("No link could be generated for the given parameters",
                    status=404)


def authenticated_view(view_function: Callable[..., ResponseValue]) -> Callable[..., ResponseValue]:
    """
    A decorator function that provides authentication for the view functions in the Flask app.

    This function checks the provided API key in the request arguments against the configured
    API keys. If the API key is valid, it executes the original view function. If not, it
    returns a 403 response.

    The decorator adds two arguments to the view function: the pool associated with the API key,
    and a boolean indicating whether the API key matches the public API key.

    :param view_function: The original view function that needs to be authenticated.
    :returns: The authenticated function, which can be called like the original view function
              but performs authentication first.
    """

    @wraps(view_function)
    def authenticated_function(*args: Any, **kwargs: Any) -> ResponseValue:
        key = request.args.get("key", None) or current_app.config[
            "PUBLIC_KEY"]  # zero-length key will be falsey and ignored
        pool = lookup_pool(key)
        if pool:
            anonymous = key == current_app.config["PUBLIC_KEY"]
        else:
            anonymous = True

        if not pool:
            return Response("The given API key was invalid.", status=403)
        return view_function(pool, anonymous, *args, **kwargs)

    return authenticated_function


def admin_view(view_function: Callable[..., ResponseValue]) -> Callable[..., ResponseValue]:
    """
    A decorator function that provides authentication for the view functions in the Flask app
    that require admin privileges.

    This function checks the provided API key against the update key. If the API key is valid,
    it executes the original view function. If not, it returns a 403 response.

    :param view_function: The original view function that needs to be authenticated.
    :returns: The authenticated function, which can be called like the original view function
              but performs authentication first.
    """

    @wraps(view_function)
    def authenticated_function(*args: Any, **kwargs: Any) -> ResponseValue:
        if (request.cookies.get("key", None) == current_app.config['UPDATE_KEY']
                or request.args.get("key", None) == current_app.config['UPDATE_KEY']
                or request.headers.get("Authorization") == f"Bearer {current_app.config['UPDATE_KEY']}"):
            resp = make_response(view_function(*args, **kwargs))
            resp.set_cookie("key",
                            current_app.config['UPDATE_KEY'],
                            max_age=datetime.timedelta(days=30),
                            samesite="strict")
            return resp
        return Response("The given API key was invalid.", status=403)

    return authenticated_function
