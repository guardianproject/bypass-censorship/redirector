FROM debian:bullseye AS portal

ENV APP="redirector"
ENV APP_BASE="/srv/"
ENV SHELL="/bin/bash"

ENV FLASK_APP="${FLASK_APP:-app}"
ENV FLASK_RUN_HOST="${FLASK_RUN_HOST:-0.0.0.0}"
ENV FLASK_RUN_PORT="${FLASK_RUN_PORT:-5000}"

ENV PYTHONPATH="/usr/lib/python3/dist-packages:/home/${APP}/.local/lib/python3.9/site-packages"
ENV PATH="/usr/local/bin:/usr/bin:/bin:/sbin:/usr/sbin:/home/${APP}/.local/bin"

ARG CONTAINER_UID="${CONTAINER_UID:-1000}"
ARG CONTAINER_GID="${CONTAINER_GID:-1000}"

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    python3-pip

RUN groupadd -r -g ${CONTAINER_GID} ${APP} && \
    useradd --no-log-init -r -u ${CONTAINER_UID} -g ${APP} ${APP} && \
    mkdir -p /home/${APP} && chown -R ${APP}. /home/${APP}
RUN mkdir -p ${APP_BASE}/${APP} && chown ${APP}. ${APP_BASE}/${APP}
USER ${APP}

WORKDIR ${APP_BASE}/${APP}
COPY --chown=${APP}:${APP} . ${APP_BASE}/${APP}

RUN pip3 install -r requirements.txt
RUN pip3 install psycopg2-binary

CMD flask run
