"""inital migration

Revision ID: 520d7ffe4e06
Revises: 
Create Date: 2023-01-21 16:31:18.463691

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '520d7ffe4e06'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('link',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('pool', sa.String(), nullable=True),
    sa.Column('origin_domain_name', sa.String(), nullable=True),
    sa.Column('url_path', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('link')
    # ### end Alembic commands ###
